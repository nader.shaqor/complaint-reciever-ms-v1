package com.intuit.complaint.receiver.rests;

import com.intuit.complaint.receiver.controller.ComplaintController;
import com.intuit.swagger.api.complaint.AddApi;
import com.intuit.swagger.model.complaint.AddNewComplaintRequest;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class ComplaintRests implements AddApi {

    private ComplaintController complaintController;

    @Override
    public ResponseEntity<Void> addNewComplaint(
            @ApiParam(value = "") @Valid @RequestBody AddNewComplaintRequest addNewComplaintRequest) {


        checkMandatoryField(addNewComplaintRequest.getUserId(), "userId");
        checkMandatoryField(addNewComplaintRequest.getPurchaseId(), "purchaseId");
        checkMandatoryField(addNewComplaintRequest.getComplaint(), "complaint");
        checkMandatoryField(addNewComplaintRequest.getSubject(), "subject");


        complaintController.addNewComplaint(addNewComplaintRequest);
        return ResponseEntity.noContent().build();
    }

    private void checkMandatoryField(String str, String fieldName) {
        if (str == null || "".equals(str)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Mandatory field is missing (" + fieldName + ")");
        }
    }

}
