package com.intuit.complaint.receiver.controller;

import com.intuit.complaint.receiver.gateway.ComplaintManagementServicesClient;
import com.intuit.complaint.receiver.models.ComplaintMessage;
import com.intuit.complaint.receiver.services.interfaces.IMessageQueueService;
import com.intuit.swagger.model.complaint.AddNewComplaintRequest;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ResponseStatusException;

@Log4j2
@Controller
@AllArgsConstructor
public class ComplaintController {

    private IMessageQueueService messageQueueService;
    private ComplaintManagementServicesClient complaintManagementServicesClient;

    public void addNewComplaint(AddNewComplaintRequest addNewComplaintRequest) {

        ComplaintMessage complaintMessage = new ComplaintMessage();
        complaintMessage.setUserId(addNewComplaintRequest.getUserId());
        complaintMessage.setSubject(addNewComplaintRequest.getSubject());
        complaintMessage.setComplaint(addNewComplaintRequest.getComplaint());
        complaintMessage.setPurchaseId(addNewComplaintRequest.getPurchaseId());

        //publish to the queue
        boolean isSuccess = messageQueueService.publishComplaint(complaintMessage);
        if (!isSuccess) {
            //try the rest call
            isSuccess = complaintManagementServicesClient.createNewComplaint(complaintMessage);
            if(!isSuccess) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "INTERNAL_SERVER_ERROR");
            }
        }
    }
}
