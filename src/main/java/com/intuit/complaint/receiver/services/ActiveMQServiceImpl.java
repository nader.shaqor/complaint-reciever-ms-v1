package com.intuit.complaint.receiver.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intuit.complaint.receiver.models.ComplaintMessage;
import com.intuit.complaint.receiver.services.interfaces.IMessageQueueService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ActiveMQServiceImpl implements IMessageQueueService {

    @Autowired
    private JmsTemplate jms;

    @Value("${spring.activemq.destinations.create-complaint}")
    private String createComplaintDestination;

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public boolean publishComplaint(ComplaintMessage complaintMessage) {
        log.debug("publishComplaint called with complaintMessage {}", complaintMessage);

        try {
            jms.convertAndSend(createComplaintDestination, objectMapper.writeValueAsString(complaintMessage));
        } catch (Exception e) {
            log.error("publishComplaint -> objectMapper.writeValueAsString failed with this error", e);
            return false;
        }

        log.debug("publishComplaint published successfully {}", complaintMessage);
        return true;
    }

}
