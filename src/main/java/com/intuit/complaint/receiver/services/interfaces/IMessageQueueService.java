package com.intuit.complaint.receiver.services.interfaces;

import com.intuit.complaint.receiver.models.ComplaintMessage;

public interface IMessageQueueService {

    boolean publishComplaint(ComplaintMessage complaintMessage);
}
