package com.intuit.complaint.receiver.gateway;

import com.intuit.complaint.receiver.gateway.interfaces.IComplaintManagementServicesClient;
import com.intuit.complaint.receiver.models.ComplaintMessage;
import com.intuit.complaint.receiver.property.ComplaintManagementClientApiProperties;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
@Log4j2
public class ComplaintManagementServicesClient implements IComplaintManagementServicesClient {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ComplaintManagementClientApiProperties clientApiProperties;

    @Override
    public boolean createNewComplaint(ComplaintMessage complaintMessage) {

        log.debug("createNewComplaint called with complaintMessage {}", complaintMessage);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<ComplaintMessage> entity = new HttpEntity<ComplaintMessage>(complaintMessage,headers);

        String apiUrl = clientApiProperties.getApiHost() + clientApiProperties.getApiPath();
        try {
             restTemplate.exchange(apiUrl, HttpMethod.POST, entity, Void.class).getBody();
        }catch (RestClientException e){
            log.error("failed while calling apiUrl with this error", e);
            return false;
        }
        return true;
    }
}
