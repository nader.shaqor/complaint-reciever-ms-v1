package com.intuit.complaint.receiver.gateway.interfaces;

import com.intuit.complaint.receiver.models.ComplaintMessage;

public interface IComplaintManagementServicesClient{

    boolean createNewComplaint(ComplaintMessage complaintMessage);
}
