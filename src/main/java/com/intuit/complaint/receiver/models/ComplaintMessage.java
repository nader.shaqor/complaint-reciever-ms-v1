package com.intuit.complaint.receiver.models;

import lombok.Data;

import java.time.Instant;

@Data
public class ComplaintMessage {
    private String userId;
    private String subject;
    private String complaint;
    private String purchaseId;
}
