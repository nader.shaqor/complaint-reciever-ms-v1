package com.intuit.complaint.receiver.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "clients.complaint-management")
@Data
public class ComplaintManagementClientApiProperties {
    private String apiHost;
    private String apiPath;
}
